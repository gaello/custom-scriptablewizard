# Custom ScriptableWizard in Unity

This repository contain simple example of how you can implement ScriptableWizard.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/09/12/custom-scriptable-wizard/

Enjoy!

---

# How to use it?

Clone this repository on your local drive and open project with Unity.

If you are interested only in seeing implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/custom-scriptablewizard/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
