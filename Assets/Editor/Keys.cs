﻿/// <summary>
/// Util class containing const string variables.
/// </summary>
public class Keys
{
    public const string AnimationsFolder = "Animations";
    public const string MaterialsFolder = "Materials";
    public const string PrefabsFolder = "Prefabs";
    public const string ResourcesFolder = "Resources";
    public const string ScriptsFolder = "Scripts";
}
